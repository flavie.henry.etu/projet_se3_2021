#Makefile pour le TP Hash Table

#Nom de l'éxécutable à créer
EXEC = main

#nom du compilo
CC = clang

#options de compil, pret pour utiliser valgrind
CFLAGS = -Wall -W -Wextra -g -O0
SRC=$(wildcard src/*.c)
DEP=$(SRC:.c=.d)
OBJ=$(SRC:.c=.o)

#chemins et nom de la lib
#LIBNAME = libfonc.a
#PATHLIBA = src/
PATHLIBH = src/

#flags de compil : le .h à la compil, la lib à l'édition de lien
#LDFLAGS = -L$(PATHLIBA)$(LIBNAME)
INCLUDES= -I$(PATHLIBH)

CFILES = $(EXEC).c
OBJS  = $(patsubst %.c,%.o,$(CFILES))

all : $(EXEC)

$(EXEC) : $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

.o: .c
	$(CC) $(CFLAGS) $(INCLUDES) -c  $<



.PHONY : clean
clean:
	rm -f ${OBJ} $(EXEC)

