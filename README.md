# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépôt `GIT` contient le projet et les données utiles pour la réalisation du projet du module de Programmation Avancée de **Flavie Henry** et **Valentin Dosias**.


## Résumé

L'objectif de ce projet est d'interroger les fichiers représentant une base de données de  58592 vols aux États Unis en 2014 par le biais de différentes requêtes. L'objectif de ce projet est de reprendre les différentes notions vu en cours de programmation avancée : structures de données complexes,lecture / écriture de fichiers, compilation séparée et automatique ... 

## Contexte

Les données sont stockées dans trois fichiers `CSV` (_comma-separated values_) qui est un format texte permettant de stocker des tableaux. Chaque ligne du fichier correspond à une ligne du tableau et les différents éléments d'une ligne sont
séparés par un élément particulier (en général une virgule `,` mais d'autres sont possibles `\t`, `;`...). La première ligne sert à décrire le nom des différents champs.
Par exemple, le fichier `flights.csv`, qui contient les vols, a la structure suivante :

~~~
MONTH,DAY,WEEKDAY,AIRLINE,ORG_AIR,DEST_AIR,SCHED_DEP,DEP_DELAY,AIR_TIME,DIST,SCHED_ARR,ARR_DELAY,DIVERTED,CANCELLED
1,1,4,WN,LAX,SLC,1625,58.0,94.0,590,1905,65.0,0,0
1,1,4,UA,DEN,IAD,823,7.0,154.0,1452,1333,-13.0,0,0
1,1,4,MQ,DFW,VPS,1305,36.0,85.0,641,1453,35.0,0,0
1,1,4,AA,DFW,DCA,1555,7.0,126.0,1192,1935,-7.0,0,0
1,1,4,WN,LAX,MCI,1720,48.0,166.0,1363,2225,39.0,0,0
~~~
Les premiers trois champs correspondent aux mois (`1` ou janvier), jour (`1`) et jour de la semaine (`4` ou mercredi), le quatrième à la compagnie aérienne suivant les codes IATA (`WN` ou Southwest Airlines Co.), `ORG_AIR` correspond à l'aéroport d'origine ou départ (`LAX`), `DEST_AIR` correspond à l'aéroport destination ou d'arrivée (`SLC`), `SCHED_DEP` correspond à l'heure de départ prévu (`1625`), `DEP_DELAY` correspond au délais de départ en minutes (`58.0`), `AIR_TIME` correspond à la durée du vol en minutes (`94.0`), `DIST` correspond à la distance en miles (`590`), `SCHED_ARR` correspond à l'heure d'arrivée prévu (`1905`), `ARR_DELAY` correspond au retard à l'arrivée en minutes (`65.0`), `DIVERTED` est un booléen qui indique si le vol a été déviée (`0`), et `CANCELLED` est un booléen qui indique si le vol a été annulé (`0`).



De la même façon, le fichier `airports.csv` décrit les aéroports et villes correspondent aux code IATA de la façon suivante :
```
IATA_CODE,AIRPORT,CITY,STATE,COUNTRY,LATITUDE,LONGITUDE
ABE,Lehigh Valley International Airport,Allentown,PA,USA,40.652359999999994,-75.4404
ABI,Abilene Regional Airport,Abilene,TX,USA,32.41132,-99.6819
ABQ,Albuquerque International Sunport,Albuquerque,NM,USA,35.04022,-106.60918999999998
```

ainsi que le fichier `airlines.csv` qui contient les codes IATA pour chaque compagnie aérienne :
```
IATA_CODE,AIRLINE
UA,United Air Lines Inc.
AA,American Airlines Inc.
US,US Airways Inc.
```

## Contenu du dépot

* __src :__ Fichiers sources et headers
    * `data.h` : Définition de toutes les constantes, structures de données et fonctions
    * `add_delete.c`: Fonctions de manipulations des différentes listes chainées
    * `find.c` : Fonctions nécessaire aux requetes `find`
    * `load_data.c` : Fonctions permettant de charger les données à partir des `CSV`
    * `main.c` : Fichier main du programme ou on peut y trouver le définition du menu
    * `most.c` : Fonctions regroupant les requetes nécessaires à toutes requetes `most`
    * `print.c` : Fonctions d'affichage des différentes structures de données
    * `read.c` : Fonctions de lecture du fichier de requêtes
    * `show.c` : Fonctions nécessaires pour les requêtes `show`
    * `data.c` : Fonctions traitant des données. Ces fonctions sont utilisées dans plusieurs requêtes

* __data : __ : Fichier `csv` et fichier type de requêtes
* __.clang-format__ : Fichier clang format issu du dépot de Mr dequidt
* __Makefile__ : Fichier servant à la compilation de projet et créant l'executable 
* __rapport_projet.pdf__ : Rapport du projet

## Définition des requêtes 



Les commandes seront les suivantes:


- `show-airports <airline_id>`  : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`
- `show-flights <port_id> <date>` : affiche les vols qui partent de l'aéroport à la date
- `most-delayed-flights`     : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines`    : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>`    : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>`    : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> (format M D)
- `avg-flight-duration <port_id> <port_id>`: calcule le temps de vol moyen entre deux aéroports
- `find-itinerary <port_id> <port_id> <date>` : trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales)
- `find-multicity-itinerary <port_id_depart> <port_id_dest1> <date> <port_id_dest2> <date> ... <port_id_dest4> <date>`: trouve un itinéraire multiville qui permet de visiter plusieurs villes (il peut y avoir des escales pour chaque vol intermediaire). Le programme est limité pour 4 villes.
- `quit`       : quit

Les fonctions `delayed` prennent toujours en comptes les retards à l'arrivée. Nous avons jugé que ce sont ces données les plus interprétables pour les utilisateurs.
Pour information, tous les paramètres sont à renseigner.
Les dates sont au format `M J` et l'heure `HH MM`
Ainsi si notre exécutable s'appelle `main` il doit être executé seul et après vous devez renseigner l'adresse du fichier de requetes.

avec le fichier `requetes.txt` qui contient par exemple:

~~~
show-airports HA
show-airlines LAX
show-flights ATL 2-26
show-flights SLC 4-17 
avg-flight-duration LAX JFK
find-itinerary PHX SAN 12 15
most-delayed-flights
most-delayed-airlines
changed-flights 5 15
find-itinerary DEN MCI 2 15 1030 
~~~
NB : contrairement au fichier de requete délivré par nos professeurs les paramètres doivent être renseigné avec seulement un espace entre eux
Le volume de données étant relativement important nous avons choisi de ne pas utiliser de tables de hachages et d'utiliser des tableaux de structures.

## Fonctionnement du programme 


0. Validation du chargement des données dans les structures
1. chargement du fichier de données après saisi du chemin
2. traitement des requêtes
4. affichage des résultats
5. revenir à l'étape 2

