#include "data.h"

void deleteCell(Cell **l) {
  Cell *temp = NULL;
  while (*l != NULL) { // tant que l'indice de fin n'est pas nul on supprime
    temp = *l;
    *l = (*l)->suiv;
    free(temp);
  }
  *l = NULL;
}

void add_head(Cell **p, char *al) {

  Cell *temp = NULL;
  temp = malloc(sizeof(Cell));
  temp->val = al; // Ici 2 ptr donc pas d'utilisation de strcpy
  temp->suiv = *p;
  *p = temp;
}

// ajoute à la liste chainée si le caractère n'est pas dedans
bool add_if_not_in(Cell **ptr, char *al) {

  if (*ptr == NULL) {
    add_head(ptr, al);
    return true;
  }
  if (strcmp((*ptr)->val, al) == 0) {
    return false;
  }
  Cell *l = NULL;
  l = *ptr;
  while (l->suiv != NULL) {
    if (strcmp(l->suiv->val, al) == 0) {
      return false;
    }
    l = l->suiv;
  }
  add_head(&(l->suiv), al);
  return true;
}

// ajoute si pas dans la liste mais version triée
void add_if_not_in_sort(Cell **ptr, char *al) {

  if (*ptr == NULL) { // si la liste est vide on ajoute
    add_head(ptr, al);
    return;
  }

  if (strcmp((*ptr)->val, al) == 0) { // l'element ajoute est le premier element
    return;
  }
  if (strcmp(al, (*ptr)->val) <
      0) { // on ajoute lorsque l'element est avant dans l'ordre alphabétique
    add_head(ptr, al);
    return;
  }
  Cell *l = NULL;
  l = *ptr;
  while (l->suiv != NULL) { // on s'arrète à l'avant dernier élément
    if (strcmp(l->suiv->val, al) ==
        0) { // si l'élément est deja dedans on ne l'ajoute pas
      return;
    }
    if (strcmp(al, l->suiv->val) < 0 &&
        strcmp(al, l->val) > 0) { // si l'élément est bien au bonne endroit dans
                                  // l'odre alphabétique
      add_head(&l->suiv, al);
      return;
    }
    l = l->suiv;
  }
  add_head(&l->suiv, al); // la liste a ete parcouru en entier on ajoute donc la
                          // valeur à la fin
}

// même principe que la fonction précédente
// return des ints permettent de compter le nombre d'éléments à ajouter
// utile dans les fonctions most pour compter le nombre d'élements ajoutés dans
// les listes chaines De cette manière on peut faire varier le nombre d'éléments
// ajoutés
int add_sort_by_value(Cell_char_value **pt, char *al, float number) {

  if (*pt == NULL) { // Si la liste est vide on ajoute
    add_head_char_value(pt, al, number);
    return 1;
  }
  if (strcmp((*pt)->val, al) == 0) { // evite les doublons
    return 0;
  }
  if (number <= (*pt)->number) {
    add_head_char_value(pt, al, number);
    return 1;
  }

  Cell_char_value *l = NULL;
  l = *pt;

  while (l->suiv != NULL) {
    if (strcmp((l->suiv)->val, al) == 0) { // evite les doublons
      return 0;
    }

    if (l->suiv->number == number) { // cas d'égalité entre number
      add_head_char_value(&l, al, number);
      return 1;
    }

    if (number < l->suiv->number && number >= l->number) {
      // le nombre est supérieur au précédent et inférieur au suivant on peut
      // l'ajouter
      add_head_char_value(&l->suiv, al, number);
      return 1;
    }
    l = l->suiv;
  }
  // l'élément n'a pas été ajouter le number est donc supérieur a tous les
  // autres on ajoute donc à la fin
  add_head_char_value(&l->suiv, al, number);

  return 1;
}

void add_head_char_value(Cell_char_value **p, char *al, float number) {

  Cell_char_value *temp = NULL;
  temp = malloc(sizeof(Cell_char_value));
  temp->val = al;
  temp->number = number; // Ici 2 ptr donc pas d'utilisation de strcpy
  temp->suiv = *p;
  *p = temp;
}

void delete_Cell_char_number(Cell_char_value **l) {
  Cell_char_value *temp = NULL;
  temp = *l;
  *l = (*l)->suiv;
  free(temp);
}

void add_head_cell_flight(Cell_flight **p, Flight *f) {

  Cell_flight *temp = NULL;
  temp = malloc(sizeof(Cell_flight));
  temp->ptr_flight = f;
  temp->suiv = *p;
  *p = temp;
}

void delete_cell_flight(Cell_flight **l) {
  Cell_flight *temp = NULL;
  temp = *l;
  temp->ptr_flight = NULL;
  *l = (*l)->suiv;
  free(temp);
}

int add_sort_cellflight_by_delay(Cell_flight **pt, Flight *f) {

  if (*pt == NULL) { // Si la liste est vide
    add_head_cell_flight(pt, f);
    return 1;
  }
  if (f->t.arr_delay <
      (*pt)->ptr_flight->t.arr_delay) { // si le delai est inférieur  à la
                                        // première cellule on n'ajoute pas
    // cette fonction est utilisé dans le cas de most flight donc ici cela ne
    // pose pas de problème est permet de réduire le nombre d'itérations
    // si elle est utilisé dans un autre cas il faudrait la modifier pour
    // ajouter les éléments inférieurs  au début
    return 0;
  }
  Cell_flight *l = NULL;
  l = *pt;
  while (l->suiv != NULL) {
    if (f->t.arr_delay <=
        (l->suiv)
            ->ptr_flight->t.arr_delay) { // quand il est inférieur on l'ajoute
      add_head_cell_flight(&l->suiv, f);
      return 1;
    }
    l = l->suiv;
  }
  add_head_cell_flight(&l->suiv, f);
  return 1;
}

void delete_listechaine_char_number(Cell_char_value **l) {
  Cell_char_value *temp = NULL;
  while (*l != NULL) { // tant que l'indice de fin n'est pas nul on supprime
    temp = *l;
    *l = (*l)->suiv;
    free(temp);
  }
  *l = NULL;
}
void delete_listechaine_flight(Cell_flight **l) {
  Cell_flight *temp = NULL;
  while (*l != NULL) { // tant que l'indice de fin n'est pas nul on supprime
    temp = *l;
    *l = (*l)->suiv;
    free(temp);
  }
  *l = NULL;
}