#include "data.h"

void avg_flight_duration(char *o, char *d, tableauFlights *f) {
  float som = 0;
  float cpt = 0;
  float moy = 0;

  for (int i = 0; i < TAB_F; i++) { // Parcour le tableau flight
    // si un vol a le meme aeroport d'arrive et le meme aeroport de depart -> ok
    // et n'est pas annulé ou detourné
    if (strcmp(f->tf[i].ori, o) == 0 && strcmp(f->tf[i].dep, d) == 0 &&
        (f->tf[i].diverted == 0 && f->tf[i].canceled == 0)) {
      som = som + f->tf[i].t.airtime;
      cpt = cpt + 1; // on compte le nombre de vol
    }
  }
  moy = som / cpt;
  if (moy != 0) {
    printf("average: %f minutes (%f flights)\n", moy, cpt);

  } else { // Moyenne à zero impossible donc le vol n'existe pas
    printf("ce trajet n'existe pas ");
  }
}

float average_delayed_airline(char *iata_code, tableauFlights *tableauflights) {
  float sum = 0;
  float number_flight = 0;
  for (int i = 0; i < TAB_F; i++) {
    // airline correcte, le vol n'est pas détourné ou annulé
    // un vol à l'heure ne compte pas pour la moyenne des retards à l'arrivé
    if (strcmp(iata_code, tableauflights->tf[i].line) == 0 &&
        tableauflights->tf[i].diverted == 0 &&
        tableauflights->tf[i].canceled == 0 &&
        tableauflights->tf[i].t.arr_delay >= 0) {
      sum = sum + tableauflights->tf[i].t.arr_delay;
      number_flight++;
    }
  }
  return sum / number_flight;
}

// Compte le nombre de delay
int count_delay(tableauFlights *f, char *ap, char *al) {
  int cpt = 0;
  for (int i = 0; i < TAB_F; ++i) {
    // si un vol est bien en retard sans être detourné ou annulé alors on le
    // compte comme un retard
    if (strcmp(ap, f->tf[i].dep) == 0 && strcmp(al, f->tf[i].line) == 0 &&
        f->tf[i].t.arr_delay > 0 && f->tf[i].diverted == 0 &&
        f->tf[i].canceled == 0) {
      cpt++;
    }
  }
  return cpt;
}
