#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define I_CODE_P 4
#define NAME 100
#define I_CODE_N 3
#define CITY 15
#define MAX_STRING 200
#define TAB_F 58492
#define TAB_L 14
#define TAB_P 322
#define NUMBER 20
#define WIDTH_AIRPORT 7
#define WIDTH_AIRLINE 2
#define WIDTH_FLIGHT 14
#define NBR_JOURS (12 * 31)
#define JOURS 31
#define NBR_MAX 5
#define NBR_MAX2 3
#define INDICE_IATACODE 0
#define INDICE_AIRLINE 1

#define INDICE_AIRPORT 1
#define INDICE_CITY 2
#define INDICE_STATE 3
#define INDICE_COUNTRY 4
#define INDICE_LATITUDE 5
#define INDICE_LONGITUDE 6

#define INDICE_MONTH 0
#define INDICE_DAY 1
#define INDICE_WEEKDAY 2
#define INDICE_FLIGHT_AIRLINE 3
#define INDICE_ORG_AIR 4
#define INDICE_DEST_AIR 5
#define INDICE_SCHED_DEP 6
#define INDICE_DEP_DELAY 7
#define INDICE_AIR_TIME 8
#define INDICE_DIST 9
#define INDICE_SHCED_ARR 10
#define INDICE_ARR_DELAY 11
#define INDICE_DIVERTED 12
#define INDICE_CANCELLED 13

#define CASE1 1
#define CASE2 2
#define CASE3 3
#define CASE4 4
#define CASE5 5
#define CASE6 6
#define CASE7 7
#define CASE8 8
#define CASE9 9
#define CASE10 10
#define CASE11 11
#define CASE12 12
#define CASE13 13

#define LECTURE_FICHIER 1000 //
typedef struct {

  char city[CITY];
  char state[I_CODE_N];
  char country[I_CODE_P];
  char lat[NUMBER];
  char lon[NUMBER];

  // long double lat; on les considère comme des chaines de caractères et si
  // besoin on changera long double lon;

} Location;

typedef struct {
  char code_p[I_CODE_P];
  char nom[NAME];
  Location loc;

} Airport;

typedef struct {

  char code_n[I_CODE_N];
  char aline[NAME];

} Airline;

typedef struct {

  int sched_dep;
  float dep_delay;
  float airtime;
  int dist;
  int arr;
  float arr_delay;

} Time;

typedef struct {
  int month;
  int day;
  int weekday;
} Date;

typedef struct {

  Date flight_date;
  char line[I_CODE_N];
  char ori[I_CODE_P];
  char dep[I_CODE_P];
  // Airline line;
  // Airport ori;
  // Airport dep;
  Time t;
  int diverted;
  int canceled;

} Flight;

typedef struct {
  Flight tf[TAB_F];

} tableauFlights;

typedef struct {
  Airline tl[TAB_L];

} tableauAL;

typedef struct {
  Airport tp[TAB_P];

} tableauAP;

typedef struct {

  int first;
  int last;

} date_index;

typedef struct {
  date_index index[NBR_JOURS + 1]; // Tableau qui commence à zéro
} calendar_index;
struct cell {

  char *val;
  struct cell *suiv;
};
typedef struct cell Cell;

struct cell_char_value {
  char *val;
  float number;
  struct cell_char_value *suiv;
};
typedef struct cell_char_value Cell_char_value;

struct cell_flight {
  Flight *ptr_flight;
  struct cell_flight *suiv;
};
typedef struct cell_flight Cell_flight;
//-------------------------//
// Liste des fonctions disponibles dans data.c

// Affiche le temps de vol moyen entre deux aéroports
void avg_flight_duration(char *, char *, tableauFlights *);

// Calcule la moyenne de retard à larrivé d'une airline ;
float average_delayed_airline(char *i, tableauFlights *);

// trouve un itinéraire multiville qui permet de visiter plusieurs villes (il
// peut y avoir des escales pour chaque vol intermediaire)

// compte le nombre de retard à l'arrivée pour un aéroport donnée
int count_delay(tableauFlights *, char *, char *);

//----------------------------------//

// Liste des fonctions dans le fichier show.c

// Fonction affiche aéroport où compagnie opère des vols
void show_airports(char *, tableauFlights *, tableauAP *);

// Fonction affiche les compagnies aérienne qui depuis un aéroport X
void show_airlines(char *, tableauFlights *, tableauAL *);

// Fonction affiche les vols qui partent de l'aéroport à une certaine date
void show_flights(char *, int, int, tableauFlights *, calendar_index *);

//----------------------------------//

////Liste des fonctions dans le fichier load_data.c

// Charge les donnés du fichier airports.csv
void data_airport(int, char *, Airport *);

// Charge les donnés du fichier airlines.csv
void data_airlines(int, char *, Airline *);

// Charge les donnés du fichier flights.csv
void data_flight(int, char *, Flight *);
// Vérifie que l'on peut ouvrir un fichier
bool open_file(FILE *);

// Charge les données d'un csv pour séléctionner lequel il faut le faire
// avec la largeur des case voir les constantes au dessus
bool load_data(FILE *, int, tableauAP *, tableauAL *, tableauFlights *);

// Permet de charger toutes les données des 3 csv
bool load_alldata(tableauAP *, tableauAL *, tableauFlights *);

// Permet de ne pas parcourir flight en entier
// recupère les indices ou un jour commence dans flighs.csv et ou il  finit
void initialize_calendar_index(tableauFlights *, calendar_index *);
// calcul le numéro de jour de l'année
int number_day(int, int);

// Permet de detecter si la ligne d'un csv comporte un problème
int detection_bug_flight(char *);

// Traite le chargement des lignes dont il n'y a pas toute les données
void load_data_flight_bug(Flight *, char *line);

//----------------------------------//

////Liste des fonctions dans print.c

// affiche les données d'un airport
void print_airport(Airport);

// Affiche les données d'un tableau d'airport
void print_tableau_airport(tableauAP);

// Affiche les données d'une airline
void print_airline(Airline);

// Affiche les données d'un tableau d'airline
void print_tableau_airline(tableauAL);

// Affiche les données d'un tableau flight
void print_tableau_flight(tableauFlights);

// Affiche le mois la semaine et le jour de la semaine d'un vol
void print_date(Date);

// Affiche les données liées au temps de vol
void print_time(Time);

// Affiche toutes les données d'un vol
void print_flight(Flight);

// Associe code_p avec un aéroport et elle l'affiche
void print_airport_found(char *, tableauAP *);

// Associe code_n avec une compagnie aérienne et elle l'affiche
void print_airline_found(char *, tableauAL *);

void print_listechaine(Cell *);

// Affiche un airport dont l'indice est recuperé par dichotomie
void print_airport_found_dicho(char *, tableauAP *);

void print_listechaine_airport(Cell *, tableauAP *);

void print_listechaine_airline(Cell *, tableauAL *);

// cherche et affiche le vol sans escale
void print_exist_direct(char *, char *, int, int, tableauFlights *,
                        calendar_index *);

// Affiche une liste chaine de cell_char_value
void print_listechaine_char_value(Cell_char_value *);

// print une liste chaine de char et value dont le char est un airport et le
// number à definir dans l'appel de la fonction
void print_listechaine_char_value_airlines(Cell_char_value *, char *,
                                           tableauAL *);

void print_listechaine_flight(Cell_flight *);
// supprime une cellule d'une liste chaine de flight;

// recherche dicho par IATA_CODE dans un tableau airport
// retourne l'indice trouvé
int recherche_dicho_airport(tableauAP *, char *);

// Affiche les vols déviés ou annulés à une certaine date
void changed_flights(int, int, tableauFlights *, calendar_index *);

//----------------------------------//

// Liste des fonctions dans le fichiers most.c

// Affiche les 5 airlines avec le retard moyen le plus élevé
void most_delayed_airlines(tableauAL *, tableauFlights *);

// Deuxieme version avec la structure de liste chaine de char value
void most_delayed_airlines_at_airport_v2(char *, tableauFlights *, tableauAL *);

// retourne les 5 vols avec le plus de retard à l'arrivé
void most_delayed_flight(tableauFlights *);

// affiche les 3 compagnies aériennes ayant le plus de retard à l'arrivée d'un
// aéroport donné
void most_delayed_airlines_at_airport(char *, tableauFlights *, tableauAL *);

// Liste des fonctions dans add_delete.c

// Fonction ajout_tete
void add_head(Cell **, char *);

// Fonction ajout si pas dans la structure
bool add_if_not_in(Cell **, char *);

// Enleve une cellule
void deleteCell(Cell **);

// Fonction ajout si pas dans la structure en version trié par ordre
// alphabétique
void add_if_not_in_sort(Cell **, char *);

// Ajoute trié par value dans la structure cell_char_value
int add_sort_by_value(Cell_char_value **, char *, float);

// Ajoute tete dans la structure cell_char_value
void add_head_char_value(Cell_char_value **, char *, float);

// Delete la première cellule d'une liste chaine de char value
void delete_Cell_char_number(Cell_char_value **);

// ajout tete dans une liste chainée  de flight
void add_head_cell_flight(Cell_flight **, Flight *);

// print une liste chaine de flight

void delete_cell_flight(Cell_flight **);
// ajoute des flight de manière trié par retard d'arrivé
int add_sort_cellflight_by_delay(Cell_flight **, Flight *);
// Supprime une liste chaine de char number
void delete_listechaine_char_number(Cell_char_value **);

// Supprime une liste chaine de flight
void delete_listechaine_flight(Cell_flight **);

//----------------------------------//

// Liste des fonctions dans read.c

// permet de lire la fonction show-flights du ficher requetes
void read_function_flight(int *, char *, tableauFlights *, calendar_index *,
                          int *, Flight **);

// permet de lire la fonction avg_flight_duration du ficher requetes
void read_function_avg(int *, char *, tableauFlights *, Flight **, int *);

// permet de lire la fonction changed_flight du fichier requetes
void read_function_changed(int *, char *, tableauFlights *, calendar_index *,
                           Flight **, int *);

// permet de lire la fonction find_itinerary à partir du fichier requetes
void read_function_itinerary(int *, char *, tableauFlights *, calendar_index *,
                             Flight **, int *);

// permet de lire la fonction find_multicity à partir du fichier requetes
void read_function_multicity(int *, char *, tableauFlights *, calendar_index *,
                             Flight **, Flight **, Flight **, Flight **, int *);

// Affiche les requetes du fichier requetes.txt
void read_requests_file(FILE *, tableauFlights, calendar_index, tableauAL,
                        tableauAP);

// renvoie un si il faut quitter
int quit(char *);

//----------------------------------//

// Liste des fonctions dans le fichier find.c

// Affiche un ou plusieurs itinéraires entre deux aéroports à une date donnée
void find_itinerary(char *, char *, int, int, tableauFlights *,
                    calendar_index *);
void find_multicity_itinerary(char *, char *, int, int, char *, int, int,
                              char *, int, int, char *, int, int,
                              tableauFlights *, calendar_index *);

// cherche si le vol existe en direct
bool exist_direct(char *, char *, int, int, tableauFlights *, calendar_index *);

// le vol exite avec des escales le même jour
bool stopover(char *, char *, int, int, tableauFlights *, calendar_index *);