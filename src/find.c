#include "data.h"

void find_itinerary(char *o, char *des, int m, int d, tableauFlights *f,
                    calendar_index *c) {
  int cpt = 0;
  int day_index = number_day(d, m);
  // on parcourt tous les vols de cette journée
  for (int i = c->index[day_index].first; i <= c->index[day_index].last; i++) {
    for (int j = c->index[day_index].first; j <= c->index[day_index].last;
         j++) {
      // Si c'est le bon vol et qu'il est chronologiquement après sans être
      // annulé c'est correct
      if (strcmp(f->tf[i].ori, o) == 0 &&
          strcmp(f->tf[i].dep, f->tf[j].ori) == 0 &&
          f->tf[i].t.arr < f->tf[j].t.sched_dep &&
          strcmp(f->tf[j].dep, des) == 0 &&
          (f->tf[i].diverted == 0 && f->tf[i].canceled == 0 && cpt == 0)) {
        // Affiche
        print_flight(f->tf[i]);
        print_flight(f->tf[j]);
        cpt = 1;
      }
    }
  }
}

void find_multicity_itinerary(char *o, char *des, int m1, int d1, char *des2,
                              int m2, int d2, char *des3, int m3, int d3,
                              char *des4, int m4, int d4, tableauFlights *f,
                              calendar_index *c) {

  // s'il existe un vol en direct on affiche sinon on vérifie avec une escale et
  // on affiche, 4x car 4 villes
  if (exist_direct(o, des, m1, d1, f, c) == 1) {
    print_exist_direct(o, des, m1, d1, f, c);
  } else if (stopover(o, des, m1, d1, f, c) == 1) {
    find_itinerary(o, des, m1, d1, f, c);
  }
  if (exist_direct(des, des2, m2, d2, f, c) == 1) {
    print_exist_direct(des, des2, m2, d2, f, c);
  } else if (stopover(des, des2, m2, d2, f, c) == 1) {
    find_itinerary(des, des2, m2, d2, f, c);
  }
  if (exist_direct(des2, des3, m3, d3, f, c) == 1) {
    print_exist_direct(des2, des3, m3, d3, f, c);
  } else if (stopover(des2, des3, m3, d3, f, c) == 1) {
    find_itinerary(des2, des3, m3, d3, f, c);
  }
  if (exist_direct(des3, des4, m4, d4, f, c) == 1) {
    print_exist_direct(des3, des4, m4, d4, f, c);
  } else if (stopover(des3, des4, m4, d4, f, c) == 1) {
    find_itinerary(des3, des4, m4, d4, f, c);
  }
}

bool stopover(char *o, char *des, int m, int d, tableauFlights *f,
              calendar_index *c) {
  int cpt = 0;
  int day_index = number_day(d, m);
  // on parcourt à une certaine date
  for (int i = c->index[day_index].first; i <= c->index[day_index].last; i++) {
    for (int j = c->index[day_index].first; j <= c->index[day_index].last;
         j++) {
      // on parcourt si c'est le bon vol et qu'il est chronologiquement après
      // sans être annulé c'est correct
      if (strcmp(f->tf[i].ori, o) == 0 &&
          strcmp(f->tf[i].dep, f->tf[j].ori) == 0 &&
          f->tf[i].t.arr < f->tf[j].t.sched_dep &&
          strcmp(f->tf[j].dep, des) == 0 &&
          (f->tf[i].diverted == 0 && f->tf[i].canceled == 0 && cpt == 0)) {

        return true;
      }
    }
  }
  return false;
}

bool exist_direct(char *o, char *des, int m, int d, tableauFlights *f,
                  calendar_index *c) {

  int day_index = number_day(d, m);
  // on parcourt tous les vols de cette journée et si aucun ne correspond
  // correspond à la destination -> false
  for (int i = c->index[day_index].first; i <= c->index[day_index].last; i++) {
    if (strcmp(f->tf[i].ori, o) == 0 && strcmp(f->tf[i].dep, des) == 0) {
      return true;
    }
  }
  return false;
}
