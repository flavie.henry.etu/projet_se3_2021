#include "data.h"

// Ici sont regroupés les fonctions permettant de recupérer les données à partir
// du csv
void data_airport(int i, char *token, Airport *ptr_airport) {
  // Si le token n'est pas null la ième donnée est mise à la données
  // correspondantes en la convertissant avec le bon type
  if (token != NULL) {
    switch (i) {
    case INDICE_IATACODE:
      strcpy(ptr_airport->code_p, token);
    case INDICE_AIRPORT:
      strcpy(ptr_airport->nom, token);
    case INDICE_CITY:
      strcpy(ptr_airport->loc.city, token);
    case INDICE_STATE:
      strcpy(ptr_airport->loc.state, token);
    case INDICE_COUNTRY:
      strcpy(ptr_airport->loc.country, token);
    case INDICE_LATITUDE:
      strcpy(ptr_airport->loc.lat, token);
    case INDICE_LONGITUDE:
      strcpy(ptr_airport->loc.lon, token);
    }
  }
}

void data_airlines(int i, char *token, Airline *ptr_airlines) {
  // Si le token n'est pas null la ième donnée est mise à la données
  // correspondantes en la convertissant avec le bon type
  switch (i) {
  case INDICE_IATACODE:
    strcpy(ptr_airlines->code_n, token);
  case INDICE_AIRLINE:
    strcpy(ptr_airlines->aline, token);
  }
}

void data_flight(int i, char *token, Flight *ptr_flight) {
  // Si le token n'est pas null la ième donnée est mise à la données
  // correspondantes en la convertissant avec le bon type
  switch (i) {
  case INDICE_MONTH:
    ptr_flight->flight_date.month = atoi(token);
  case INDICE_DAY:
    ptr_flight->flight_date.day = atoi(token);
  case INDICE_WEEKDAY:
    ptr_flight->flight_date.weekday = atoi(token);
  case INDICE_FLIGHT_AIRLINE:
    strcpy(ptr_flight->line, token);
  case INDICE_ORG_AIR:
    strcpy(ptr_flight->ori, token);
  case INDICE_DEST_AIR:
    strcpy(ptr_flight->dep, token);
  case INDICE_SCHED_DEP:
    ptr_flight->t.sched_dep = atoi(token);
  case INDICE_DEP_DELAY:
    ptr_flight->t.dep_delay = atof(token);
  case INDICE_AIR_TIME:
    ptr_flight->t.airtime = atof(token);
  case INDICE_DIST:
    ptr_flight->t.dist = atoi(token);
  case INDICE_SHCED_ARR:
    ptr_flight->t.arr = atoi(token);
  case INDICE_ARR_DELAY:
    ptr_flight->t.arr_delay = atof(token);
  case INDICE_DIVERTED:
    ptr_flight->diverted = atoi(token);
  case INDICE_CANCELLED:
    ptr_flight->canceled = atoi(token);
  }
}

bool open_file(FILE *file) {
  // Permet de vérifier qu'un fichier est bien ouvert
  if (file == NULL) {
    return 0;
  }
  return 1;
}

// Le paramètres Width_csv nous permet de choisir quel fichier doit être lu
// Permet d'avoir une seule fonction au lieu de 3 différentes-> moins de
// répétition de code
bool load_data(FILE *file, int width_csv, tableauAP *tableau_airport,
               tableauAL *tableau_airline, tableauFlights *tableau_flight) {
  if (file == NULL) {
    return 0;
  }
  char line[MAX_STRING];
  int indice_tableau = -1; // on initialise à -1 pour pas lire la première
                           // ligne qui n'est pas utile pour les requêtes
  while (fgets(line, sizeof(line), file) != NULL) // le fichier peut etre lu
  {
    int bug = -1;
    // Si on parcoure le csv flight on vérifié que la ligne est complète

    if (width_csv == WIDTH_FLIGHT) {
      bug = detection_bug_flight(line); //
      if (bug == 1) {
        load_data_flight_bug(&(tableau_flight->tf[indice_tableau]), line);
        // On charge avec la fonction spécifique que quand il y a un bug
        // Complexité de cette fonction plus élévé que la boucle suivante
      }
    }

    char *token = NULL;        // pointeur de chaine de caractère
    token = strtok(line, ","); // CSV sous format de virgule
    for (int i = 0; i < width_csv; i++) {
      if (indice_tableau != -1) {
        if (width_csv == WIDTH_AIRLINE) {
          data_airlines(i, token, &(tableau_airline->tl[indice_tableau]));
        }
        if (width_csv == WIDTH_AIRPORT) {
          data_airport(i, token, &(tableau_airport->tp[indice_tableau]));
        }
        if (width_csv == WIDTH_FLIGHT && bug == 0) {
          data_flight(i, token, &(tableau_flight->tf[indice_tableau]));
        }
        token = strtok(NULL, ","); // élément suivant
      }
    }
    indice_tableau++;
  }
  return 1;
}

int detection_bug_flight(char *line) {
  // Pas de bug lorsque diverted et cancelled sont à 0
  // En partant de la fin ce sont toujours les caractères aux deux position
  // suivantes
  int longueur = strlen(line);
  int position_diverted = longueur - 4;
  int position_canceled = longueur - 2;
  if (line[position_diverted] == '1' || line[position_canceled] == '1') {
    return 1;
    // La ligne présentera une erreur de données
  }
  return 0;
  // La ligne ne présentera pas d'erreur de données
}

void load_data_flight_bug(Flight *ptr_flight, char *line) {

  int longueur = strlen(line);
  char element[longueur];
  strcpy(element, line);
  int n = 0;

  while (n < longueur - 1)
  // on remplace chaque virgule par une fin de chaine de caractères
  {
    n++;
    if (element[n] == ',') {
      element[n] = '\0';
    }
  }
  int i = 0;
  data_flight(0, element, ptr_flight);
  n = 1;
  while (i < longueur) {
    if (element[i] == '\0') {
      if (element[i + 1] !=
          '\0') // on ne met rien si on a plusieurs virgules qui se suivent
      // evite les seg fault ou les décalages de données
      {
        data_flight(n, element + i + 1, ptr_flight);
      }
      // Si l'élement n'est pas présent on le remplace par un 0 -> les éléments
      // manquant sont tjrs des nombres
      else {
        data_flight(n, "0", ptr_flight);
      }
      n++;
    }
    i++;
  }
}

bool load_alldata(tableauAP *tableau_airport, tableauAL *tableau_airlines,
                  tableauFlights *tableau_flight) {
  // on charge d'abords les airports
  FILE *file = fopen("data/airports.csv", "r");
  if (open_file(file) == 0) { // On vérifie que le fichier est bien ouvert
    printf("Impossible d'ouvrir airports.csv");
    return 0;
  }
  // On vérifie que les données sont bien chargés
  if (load_data(file, WIDTH_AIRPORT, tableau_airport, NULL, NULL) == 0) {
    printf("Données airports non chargées");
    return 0;
  }
  printf("data airports OK\n");
  fclose(file);
  file = fopen("data/airlines.csv", "r");
  if (open_file(file) == 0) { // On vérifie que le fichier est bien ouvert
    printf("Impossible d'ouvrir airlines.csv");
    return 0;
  }
  // On vérifie que les données sont bien chargés
  if (load_data(file, WIDTH_AIRLINE, NULL, tableau_airlines, NULL) == 0) {
    printf("Données airlines non chargées");
    return 0;
  }
  printf("data airlines OK\n");
  fclose(file);
  file = fopen("data/flights.csv", "r");
  if (open_file(file) == 0) { // On vérifie que le fichier est bien ouvert
    printf("Impossible d'ouvrir flights.csv");
    return 0;
  }
  // On vérifie que les données sont bien chargés
  if (load_data(file, WIDTH_FLIGHT, NULL, NULL, tableau_flight) == 0) {
    printf("Données flights non chargées");
    return 0;
  }
  printf("data flights OK \n");
  fclose(file);
  return 1;
}
// Formule permettant de retourne le numéro de jour à partir de la date
int number_day(int day, int month) { return day + (month - 1) * JOURS; }

// Les jours ou il n'y a pas de vols sont initialisés à zero ex : 30 fevrier
// indice 0
void initialize_calendar_index(tableauFlights *tableau_flight,
                               calendar_index *calendar) {
  // Permet d'assimiler chaque date avec un indice de début et indice de fin
  // Evite de parcourir tous le tableau de flights
  calendar->index[0].first = 0;
  calendar->index[0].first = 0;
  calendar->index[1].first = 0;
  calendar->index[NBR_JOURS].last = TAB_F;
  int number = 0;
  for (int i = 1; i < TAB_F - 1; i++) {
    // a chaque fois qu'un jour change ou qu'un mois change on charge les
    // indices de début et de fin
    if (tableau_flight->tf[i].flight_date.month !=
            tableau_flight->tf[i + 1].flight_date.month ||
        tableau_flight->tf[i].flight_date.day !=
            tableau_flight->tf[i + 1].flight_date.day) {
      number = number_day(tableau_flight->tf[i].flight_date.day,
                          tableau_flight->tf[i].flight_date.month);
      calendar->index[number].last = i;
      calendar->index[number + 1].first = i + 1;
    }
  }
  printf("calendrier ok \n");
}