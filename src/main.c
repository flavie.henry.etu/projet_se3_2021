#include "data.h"

int main() {

  tableauAP tableau_airports;

  tableauAL tableau_airlines;
  tableauFlights tableau_flights;
  calendar_index calendar; // Les jours qui ne sont pas dans le calendrier
                           // ex 30/02 auront leur valeurs à zero
  int q = 0;
  char choice[MAX_STRING];
  bool open = 0;
  // On charge les données
  load_alldata(&tableau_airports, &tableau_airlines, &tableau_flights);
  // On charge la structure nous permettant de ne pas parcourir tous le tableau
  // flights
  initialize_calendar_index(&tableau_flights, &calendar);

  while (q != 1) {
    printf("Saisir le nom du fichier(ou le chemin) ou quit\n");
    scanf("%s", &choice[0]);
    q = quit(choice);
    if (q == 0) {
      //"data/requetes.txt"
      FILE *fp = fopen(choice, "r");
      open = open_file(fp);
      if (open == 1) {
        // Si le fichier est bien ouvert alors on execute les requetes
        read_requests_file(fp, tableau_flights, calendar, tableau_airlines,
                           tableau_airports);
        fclose(fp);
      } else {
        printf("Problème lors de l'ouverture du fichier\n");
      }
    }
  }
  return 0;
}
