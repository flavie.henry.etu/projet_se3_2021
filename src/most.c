#include "data.h"

// Toutes les fonctions fonctionne avec des listes chainées (différentes) et
// triés selon un paramètres
void most_delayed_airlines(tableauAL *tableau_airlines,
                           tableauFlights *tableau_flights) {
  Cell_char_value *l = NULL;
  int cpt = 0;
  float delay = 0;
  for (int i = 0; i < TAB_L;
       i++) // Les 5 premiers sont ajoutés à la liste chainées et ensuite on
  // Le compteur nous permet de s'assurer que la liste reste tjrs à 5 éléments
  {
    delay = average_delayed_airline(tableau_airlines->tl[i].code_n,
                                    tableau_flights);
    if (cpt < NBR_MAX) {
      cpt += add_sort_by_value(&l, tableau_airlines->tl[i].code_n, delay);
    } else {
      // Lorsque qu'un élément est ajouté on supprime le premier pour garder la
      // liste à 5 éléments
      int b = 0;
      b = add_sort_by_value(&l, tableau_airlines->tl[i].code_n, delay);
      if (b != 0) {
        delete_Cell_char_number(&l);
      }
    }
  }
  print_listechaine_char_value_airlines(
      l, "Retard moyen à l'arrivée : ", tableau_airlines);
  delete_listechaine_char_number(&l);
}

void most_delayed_airlines_at_airport_v2(char *app, tableauFlights *f,
                                         tableauAL *al) {
  Cell_char_value *l = NULL;
  float delay = 0;
  for (int i = 0; i < TAB_L; i++) {
    delay = 0;
    // On compte le nombre de retard
    delay = count_delay(f, app, f->tf[i].line);
    if (i < NBR_MAX2) {
      add_sort_by_value(&l, f->tf[i].line, delay);
    } else {
      // Lorsque qu'un élément est ajouté on supprime le premier pour garder la
      // liste à 3 éléments
      int b = 0;
      b = add_sort_by_value(&l, f->tf[i].line, delay);
      if (b != 0) {
        delete_Cell_char_number(&l);
      }
    }
  }
  printf(" A l'aéroport %s \n", app);
  print_listechaine_char_value_airlines(l, "Nombre de retard : ", al);
  delete_listechaine_char_number(&l);
}

/// Meme principe que la fonction most_delayed_airlines
void most_delayed_flight(tableauFlights *f) {
  Cell_flight *l = NULL;
  int cpt = 0;

  for (int i = 0; i < TAB_F; i++) {
    if (cpt < NBR_MAX) // on remplit la liste de NBR MAX element et après on
                       // supprime quand on en ajoute un
    {
      cpt += add_sort_cellflight_by_delay(&l, &f->tf[i]);
    } else {
      int ajout = 0;
      ajout = add_sort_cellflight_by_delay(&l, &f->tf[i]);
      if (ajout == 1) {
        delete_cell_flight(&l);
      }
    }
  }
  print_listechaine_flight(l);
  delete_listechaine_flight(&l);
}

// Cette fonction a été remplace par la version deux et n'est plus appelé
// COMMANDE : les 3 compagnies aériennes les plus delayed sur un aéroport
void most_delayed_airlines_at_airport(char *app, tableauFlights *f,
                                      tableauAL *al) {
  struct cell *temp = NULL;
  int max = 0;
  int indice = 0;
  // Recupère le max
  for (int i = 0; i < TAB_L; i++) {

    if (max < count_delay(f, app, f->tf[i].line)) {
      max = count_delay(f, app, f->tf[i].line);
      indice = i;
    }
  }
  add_if_not_in_sort(&temp,
                     f->tf[indice].line); // ajout à la liste pour ensuite
                                          // comparer avec les autres valeurs
  // affiche le premier
  print_airline_found((f->tf[indice].line), al);
  printf("Cette compagnie a eu %d retards à l'arrivé à l'aéroport %s \n", max,
         app);
  max = 0;
  // Recherche le deuxième max
  for (int i = 0; i < TAB_L; i++) {

    if (max < count_delay(f, app, f->tf[i].line) &&
        strcmp(temp->val, f->tf[i].line) != 0) {
      max = count_delay(f, app, f->tf[i].line);
      indice = i;
    }
  }
  add_if_not_in_sort(&temp, f->tf[indice].line);
  // affiche
  print_airline_found((f->tf[indice].line), al);
  printf("Cette compagnie a eu %d retards à d'arrivé à l'aéroport %s \n", max,
         app);
  max = 0;
  // Recherche le dernier max
  for (int i = 0; i < TAB_L; i++) {

    if (max < count_delay(f, app, f->tf[i].line) &&
        strcmp(temp->val, f->tf[i].line) != 0 &&
        strcmp(temp->suiv->val, f->tf[i].line) != 0) {
      max = count_delay(f, app, f->tf[i].line);
      indice = i;
    }
  }
  print_airline_found((f->tf[indice].line), al);
  printf("Cette compagnie a eu %d retards à l'arrivé à l'aéroport %s \n", max,
         app);
}
