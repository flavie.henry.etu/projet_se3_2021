#include "data.h"

void print_airport(Airport airport) {
  printf("%s | ", airport.code_p);
  printf("%s | ", airport.nom);
  printf("%s | ", airport.loc.city);
  printf("%s | ", airport.loc.state);
  printf("%s | ", airport.loc.country);
  printf("%s | ", airport.loc.lat);
  printf("%s", airport.loc.lon);
  printf("\n");
}

void print_tableau_airport(tableauAP tableau_airport) {
  for (int i = 0; i < TAB_P; i++) {
    print_airport(tableau_airport.tp[i]);
  }
}

void print_airline(Airline airline) {
  printf("%s |   %s", airline.code_n, airline.aline);
}

void print_tableau_airline(tableauAL tableau_airline) {
  for (int i = 0; i < TAB_L; i++) {
    print_airline(tableau_airline.tl[i]);
  }
}

void print_date(Date date) {
  printf("%d month %d day %d weekday ", date.month, date.day, date.weekday);
}

void print_time(Time t) {
  printf("%d |", t.sched_dep);
  printf("%f |", t.dep_delay);
  printf("%f |", t.airtime);
  printf("%d |", t.dist);
  printf("%d |", t.arr);
  printf("%f |", t.arr_delay);
}

void print_flight(Flight flight) {
  print_date(flight.flight_date);
  printf("%s |", flight.line);
  printf("%s |", flight.ori);
  printf("%s |", flight.dep);
  print_time(flight.t);
  printf("%d |", flight.diverted);
  printf("%d", flight.canceled);
  printf("\n");
}

void print_tableau_flight(tableauFlights tableau_flight) {
  for (int i = 0; i < TAB_F; i++) {
    print_flight(tableau_flight.tf[i]);
  }
}

// trouve les données d'un aéroport à partir de son nom
// Cette fonction n'est plus utilisé et nous utilisons la fonction par recherche
// dichotomique
void print_airport_found(char *cp, tableauAP *ap) {
  for (int i = 0; i < TAB_P; i++) {
    if (strcmp(ap->tp[i].code_p, cp) == 0) {
      print_airport(ap->tp[i]);
    }
  }
}

// trouve les données d'une compagnie aérienne à partir de son nom
// Il n'y a pas un nombre d'airline très important -> on peut parcourir à chaque
// fois tous la liste Les airline ne sont pas triés
void print_airline_found(char *cp, tableauAL *al) {
  for (int i = 0; i < TAB_L; i++) {
    if (strcmp(al->tl[i].code_n, cp) == 0) {
      print_airline(al->tl[i]);
    }
  }
}

// affiche une liste chaînée
void print_listechaine(Cell *l) {
  while (l != NULL) {
    printf("%s ", l->val);
    l = l->suiv;
  }
  printf("\n");
}
// Les airports sont triés par ordre alphabétique on peut faire une recherche
// dichotomique au lieu de tous parcourir
void print_airport_found_dicho(char *cp, tableauAP *ap) {
  int indice = recherche_dicho_airport(ap, cp);
  if (indice != -1) { // On vérifie que l'aéroport existe bien
    print_airport(ap->tp[indice]);
  }
}

void print_listechaine_airport(Cell *ptr, tableauAP *tableau_airport) {
  Cell *l = NULL;
  l = malloc(sizeof(Cell));
  l = ptr;
  while (l != NULL) {
    print_airport_found_dicho(l->val, tableau_airport);
    l = l->suiv;
  }
  printf("\n");
  free(l);
}

void print_listechaine_airline(Cell *ptr, tableauAL *tableau_airline) {
  Cell *l = malloc(sizeof(Cell));
  l = ptr;
  while (l != NULL) {
    print_airline_found(l->val, tableau_airline);
    l = l->suiv;
  }
  printf("\n");
  free(l);
}

void print_exist_direct(char *o, char *des, int m, int d, tableauFlights *f,
                        calendar_index *c) {
  int cpt = 0;
  int day_index = number_day(d, m);
  // Parcour tous les vols de la journée
  for (int i = c->index[day_index].first; i <= c->index[day_index].last; i++) {
    // depart et arrive correspont à la demande et le vol n'a pas deja été
    // trouvé
    if (strcmp(f->tf[i].ori, o) == 0 && strcmp(f->tf[i].dep, des) == 0 &&
        cpt == 0) {
      print_flight(f->tf[i]); // affiche le vol possible en direct
      cpt = 1;
    }
  }
}

void print_listechaine_char_value(Cell_char_value *l) {
  while (l != NULL) {
    printf("%s ", l->val);
    printf("%f ", l->number);
    printf("\n");
    l = l->suiv;
  }
}

void print_listechaine_char_value_airlines(Cell_char_value *ptr,
                                           char *unite_number,
                                           tableauAL *tableau_airline) {
  Cell_char_value *l = malloc(sizeof(Cell_char_value));
  l = ptr;
  while (l != NULL) {
    printf("Compagnie : ");
    print_airline_found(l->val, tableau_airline);
    printf("%s : %f \n", unite_number, l->number);
    l = l->suiv;
  }
  free(l);
}

void print_listechaine_flight(Cell_flight *ptr) {
  Cell_flight *l = malloc(sizeof(Cell_flight));
  l = ptr;
  while (l != NULL) {
    print_flight(*(l->ptr_flight));
    l = l->suiv;
  }
  free(l);
}
int recherche_dicho_airport(tableauAP *tableau_airport, char *research) {
  // Algorithme de recherche dicho par ordre alphabétique
  int inf = 0;
  int sup = TAB_P;
  int middle = 0;
  while (inf <= sup) {
    middle = (inf + sup) / 2;
    if (strcmp(tableau_airport->tp[middle].code_p, research) > 0) {
      sup = middle - 1;
    }
    if (strcmp(tableau_airport->tp[middle].code_p, research) < 0) {
      inf = middle + 1;
    }
    if (strcmp(tableau_airport->tp[middle].code_p, research) == 0) {
      return middle;
    }
  }
  // L'élément n'a pas été trouvé on retourne -1
  return -1;
}

void changed_flights(int m, int d, tableauFlights *f,
                     calendar_index *calendar) {
  int cpt = 0;
  int day_index = number_day(d, m);
  for (int i = calendar->index[day_index].first;
       i <= calendar->index[day_index].last; i++) {
    // Si les vols ont été annules ou dévies on les prints
    if ((f->tf[i].diverted == 1 || f->tf[i].canceled == 1) &&
        f->tf[i].flight_date.day == d && f->tf[i].flight_date.month == m) {
      print_flight(f->tf[i]);
      cpt = 1;
    }
  }
  if (cpt == 0) {
    printf("Aucun vol n'a été dévié ou annulé ce %d/%d \n", d, m);
  }
}
