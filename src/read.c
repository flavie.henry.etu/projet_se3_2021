#include "data.h"

// Les fonctions read permettent de lire les fichiers requetes

void read_function_flight(int *i, char *token, tableauFlights *f,
                          calendar_index *c, int *cpt, Flight **fl) {

  switch (*i) {
    // On récupère d'abord l'aeroport d'origine
  case CASE1:
    strcpy((*fl)->ori, token);
    break;

  case CASE2:
    // On récupère le mois
    (*fl)->flight_date.month = atoi(token);

    break;
  case CASE3:
    printf("show-flights %s, %d, %d \n", (*fl)->ori, (*fl)->flight_date.month,
           atoi(token));
    show_flights((*fl)->ori, (*fl)->flight_date.month, atoi(token), f, c);
    *cpt = 0;
    *i = -1;
    break;
  }
}
void read_function_avg(int *i, char *token, tableauFlights *f, Flight **fl,
                       int *cpt) {
  switch (*i) {
  case CASE1:
    // Récupère les paramètres de recherche
    strcpy((*fl)->ori, token);

    break;

  case CASE2:
    // Execution de la requete
    printf("avg-flight-duration %s, %s \n", (*fl)->ori, token);
    avg_flight_duration((*fl)->ori, token, f);
    printf("\n");
    *cpt = 0;
    *i = -1;
    break;
  }
}

void read_function_changed(int *i, char *token, tableauFlights *f,
                           calendar_index *c, Flight **fl, int *cpt) {

  switch (*i) {
  case CASE1:
    (*fl)->flight_date.month = atoi(token);
    // Récupère les paramètres de recherche
    break;

  case CASE2:
    // Execution de la requete
    printf("changed_flights %d - %d \n", (*fl)->flight_date.month, atoi(token));
    changed_flights((*fl)->flight_date.month, atoi(token), f, c);
    printf("\n");
    *cpt = 0;
    *i = -1;
    break;
  }
}

void read_function_itinerary(int *i, char *token, tableauFlights *f,
                             calendar_index *c, Flight **fl, int *cpt) {

  switch (*i) {
  case CASE1:
    strcpy((*fl)->ori, (token));

    break;
  case CASE2:
    strcpy((*fl)->dep, (token));
    break;
  case CASE3:
    (*fl)->flight_date.month = atoi(token);

    break;
  case CASE4:
    printf("find-itinerary  %s  %s %d-%d \n", (*fl)->ori, (*fl)->dep,
           (*fl)->flight_date.month, atoi(token));
    find_itinerary((*fl)->ori, (*fl)->dep, (*fl)->flight_date.month,
                   atoi(token), f, c);
    printf("\n");
    *cpt = 0;
    *i = -1;
    break;
  }
}

void read_function_multicity(int *i, char *token, tableauFlights *f,
                             calendar_index *c, Flight **fl, Flight **fl2,
                             Flight **fl3, Flight **fl4, int *cpt) {

  switch (*i) {
  case CASE1:
    strcpy((*fl)->ori, (token));

    break;
  case CASE2:
    strcpy((*fl)->dep, (token));
    break;
  case CASE3:
    (*fl)->flight_date.month = atoi(token);

    break;
  case CASE4:
    (*fl)->flight_date.day = atoi(token);

    break;
  case CASE5:
    strcpy((*fl2)->ori, (token));

    break;

  case CASE6:
    (*fl2)->flight_date.month = atoi(token);

    break;
  case CASE7:
    (*fl2)->flight_date.day = atoi(token);

    break;
  case CASE8:
    strcpy((*fl3)->ori, (token));

    break;

  case CASE9:
    (*fl3)->flight_date.month = atoi(token);

    break;
  case CASE10:
    (*fl3)->flight_date.day = atoi(token);

    break;
  case CASE11:
    strcpy((*fl4)->ori, (token));

    break;

  case CASE12:
    (*fl4)->flight_date.month = atoi(token);

    break;
  case CASE13:
    printf("find-multicity-itinerary %s %s %d-%d %s %d-%d %s %d-%d %s %d-%d\n",
           (*fl)->ori, (*fl)->dep, (*fl)->flight_date.month,
           (*fl)->flight_date.day, (*fl2)->ori, (*fl2)->flight_date.month,
           (*fl2)->flight_date.day, (*fl3)->ori, (*fl3)->flight_date.month,
           (*fl3)->flight_date.day, (*fl4)->ori, (*fl4)->flight_date.month,
           atoi(token));
    find_multicity_itinerary((*fl)->ori, (*fl)->dep, (*fl)->flight_date.month,
                             (*fl)->flight_date.day, (*fl2)->ori,
                             (*fl2)->flight_date.month, (*fl2)->flight_date.day,
                             (*fl3)->ori, (*fl3)->flight_date.month,
                             (*fl3)->flight_date.day, (*fl4)->ori,
                             (*fl4)->flight_date.month, atoi(token), f, c);
    printf("\n");
    // Remise à 0

    *cpt = 0;
    *i = -1;
    break;
  }
}

void read_requests_file(FILE *fp, tableauFlights tableau_flights,
                        calendar_index calendar, tableauAL tableau_airlines,
                        tableauAP tableau_airports) {
  Flight *f = malloc(sizeof(Flight));
  Flight *f2 = malloc(sizeof(Flight));
  Flight *f3 = malloc(sizeof(Flight));
  Flight *f4 = malloc(sizeof(Flight));
  char lignes[LECTURE_FICHIER];
  char *t = "show-airports";
  char *p = "show-airlines";
  char *u = "show-flights";
  char *m = "most-delayed-airlines-by-airports";
  char *y = "avg-flight-duration";
  char *a = "changed-flights";
  char *b = "find-itinerary";
  char *d = "find-multicity-itinerary";
  char *r = "delayed-airline";
  char *k = "most-delayed-airlines";
  char *z = "most-delayed-flights";

  int i = -1;
  int cpt = 0;

  while (fgets(lignes, LECTURE_FICHIER, fp) != NULL) {
    char *token = NULL;

    token = strtok(lignes, "  \n"); // on prend en compte les tabulations

    while (token) {

      if (strcmp(token, t) == 0) {

        cpt = CASE1;
      }
      if (strlen(token) == CASE2 && cpt == CASE1) {

        printf("show-airports %s\n", token);
        show_airports(token, &tableau_flights, &tableau_airports);

        cpt = 0;
      }
      if (strcmp(token, p) == 0) {

        cpt = CASE2;
      }
      if (strlen(token) == CASE3 && cpt == CASE2) {

        printf("show-airlines %s\n", token);
        show_airlines(token, &tableau_flights, &tableau_airlines);
        cpt = 0;
      }
      if (strcmp(token, u) == 0) {

        cpt = CASE3;
      }
      if (cpt == CASE3) {
        i++;
        read_function_flight(&i, token, &tableau_flights, &calendar, &cpt, &f);
        printf("\n");
      }

      if (strcmp(token, m) == 0) {

        cpt = CASE4;
      }
      if (strlen(token) == CASE3 && cpt == CASE4) {

        printf("most-delayed-airlines-by-airports %s\n", token);
        most_delayed_airlines_at_airport_v2(token, &tableau_flights,
                                            &tableau_airlines);

        cpt = 0;
      }

      if (strcmp(token, y) == 0) {

        cpt = CASE5;
      }
      if (cpt == CASE5) {
        i++;
        read_function_avg(&i, token, &tableau_flights, &f, &cpt);
        printf("\n");
      }

      if (strcmp(token, a) == 0) {

        cpt = CASE6;
      }
      if (cpt == CASE6) {
        i++;

        read_function_changed(&i, token, &tableau_flights, &calendar, &f, &cpt);
        printf("\n");
      }

      if (strcmp(token, b) == 0) {

        cpt = CASE7;
      }
      if (cpt == CASE7) {
        i++;

        read_function_itinerary(&i, token, &tableau_flights, &calendar, &f,
                                &cpt);
      }
      if (strcmp(token, d) == 0) {

        cpt = CASE8;
      }
      if (cpt == CASE8) {
        i++;

        read_function_multicity(&i, token, &tableau_flights, &calendar, &f, &f2,
                                &f3, &f4, &cpt);
      }
      if (strcmp(token, r) == 0) {

        cpt = CASE9;
      }
      if (strlen(token) == CASE2 && cpt == CASE9) {

        printf("delayed-airline %s ", token);
        printf("%f \n", average_delayed_airline(token, &tableau_flights));
        printf("\n");
        cpt = 0;
      }
      if (strcmp(token, k) == 0) {

        printf("most-delayed-airlines\n");
        most_delayed_airlines(&tableau_airlines, &tableau_flights);
        printf("\n");
      }
      if (strcmp(token, z) == 0) {

        printf("most-delayed-fights\n");
        most_delayed_flight(&tableau_flights);
        printf("\n");
      }

      token = strtok(NULL, " \n");
    }
  }
  free(f);
  free(f2);
  free(f3);
  free(f4);
}

// Permet de savoir quand l'utilisateur veut arreter les requetes
int quit(char *word) {
  if (strcmp(word, "quit") == 0) {
    return 1;
  }
  return 0;
}
