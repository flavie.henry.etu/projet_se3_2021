#include "data.h"

void show_flights(char *cp, int m, int d, tableauFlights *f,
                  calendar_index *calendar) {
  int day_index = number_day(d, m);
  // Parcour le tableau à la date données
  for (int i = calendar->index[day_index].first;
       i <= calendar->index[day_index].last; i++) {
    // Si l'aerport d'origine correspond -> print
    if (strcmp(f->tf[i].ori, cp) == 0) {
      print_flight(f->tf[i]);
    }
  }
}

void show_airports(char *al, tableauFlights *f, tableauAP *ap) {
  Cell *p = NULL;
  // Construction de la liste chaine
  // On ne connait pas la taille de la liste
  for (int i = 0; i < TAB_F; i++) {
    if (strcmp(al, f->tf[i].line) == 0) {
      // Pour réduire la compléxité  moyenne on raisonne par liste chainé triée
      add_if_not_in_sort(&p, f->tf[i].ori);
      add_if_not_in_sort(&p, f->tf[i].dep);
    }
  }
  // Impression de la liste
  print_listechaine_airport(p, ap);
  // On vide la liste
  deleteCell(&p);
}

void show_airlines(char *cp, tableauFlights *f, tableauAL *al) {
  Cell *p = NULL;

  for (int i = 0; i < TAB_F; i++) {
    if (strcmp(cp, f->tf[i].dep) == 0) {

      add_if_not_in_sort(&p, f->tf[i].line);
    }
  }
  print_listechaine_airline(p, al);
  // On vide la liste
  deleteCell(&p);
  free(p);
}
